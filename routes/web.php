<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\CategoryController;
use App\Models\Product;

Route::get('/', [SiteController::class, 'index']);
Route::get('/product', [SiteController::class, 'show']);
Route::middleware('auth')->group(function () {
    Route::get('/home', [SiteController::class, 'home']);



    Route::prefix('admin')->name('admin.')->middleware(['auth', 'admin'])->group(function () {

        Route::get('/', [AdminController::class, 'index'])->name('index');

        Route::resource('users', UserController::class);
        Route::resource('categories', CategoryController::class);

        Route::get('products/{product}/media', [\App\Http\Controllers\Admin\ProductController::class, 'media'])->name('products.media');
        Route::put('products/{product}/media', [\App\Http\Controllers\Admin\ProductController::class, 'mediaUpload'])->name('products.upload');
        Route::delete('products/{product}/media', [\App\Http\Controllers\Admin\ProductController::class, 'removeMedia'])->name('products.removeMedia');
        Route::resource('products', ProductController::class);
    });
});
