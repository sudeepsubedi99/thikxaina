@extends('adminlte::page')

@section('title', 'Product Details')

@section('content')


<div class="card">
      <div class="card-header ">
          <h3 class="card-title text-bold">Product Information</h3>
          <div class="card-tools">
            <a class="btn btn-info btn-sm" href="{{ route('admin.products.media', $product->id) }}">
                <i class="fas fa-image fa-fw mr-1"></i> Image
            </a>

        <a href="{{ route('admin.products.index') }}" class="btn btn-primary btn-sm">
            Go Back
        </a>
          </div>
      </div>
      <div class="card-body p-0 " >

          <table class="table table-bordered table-striped">
            <tr>
                <th width="15%">ID</th>
                <td>{{ $product->id }}</td>
            </tr>
              <tr>
                  <th>Name</th>
                  <td>{{ $product->name }}</td>
              </tr>

              <tr>
                  <th>Price</th>
                  <td>{{ money($product->price) }}</td>
              </tr>
              <tr>
                <th>Description</th>
                <td>{{ $product->description }}</td>
            </tr>

            <tr>
                <th>Images</th>
                <td>
                    @foreach($product->medias as $item)
                    <img src="/storage/{{ $item->path }}" height="70px" class="mr-1">
                    @endforeach
                </td>
            </tr>


          </table>

      </div>
  </div>

  @stop
