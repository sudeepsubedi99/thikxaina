@extends('adminlte::page')

@section('title', 'Product Images')

@section('content')

    <x-alert/>
    <x-delete/>

    <div class="card">
        <div class="card-header">

            <h3 class="card-title" style="font-size:1.5em;font-weight: bold">Product Images</h3>
            <div class="card-tools">
                <a class="btn btn-success btn-sm" href="{{ route('admin.products.show', $product->id) }}">
                    <i class="fas fa-arrow-left fa-fw mr-1"></i> Go Back
                </a>
            </div>
        </div>
        <div class="card-body">

            <form method="POST" action="{{ route('admin.products.upload', $product->id) }}"
                  enctype="multipart/form-data">

                @csrf
                @method('PUT')

                <div class="form-group">
                    <label for="image">Image <span title="Required" class="text-danger">*</span></label>
                    <input
                        type="file"
                        name="image" id="image"
                        class="form-control-file @error('image') is-invalid @endif"
                    >

                    @error('image')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <input type="submit" class="btn btn-primary" value="Upload">
            </form>
        </div>
        <div class="card-body p-0">
            <table class="table table-bordered border-top-0">
                <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Path</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>

                @foreach($product->medias as $media)
                    <tr>

                        <td>{{ $media->id }}</td>
                        <td><img src="/storage/{{ $media->path }}" height="70px"></td>
                        <td>{{ $media->name }}</td>
                        <td>{{ $media->path }}</td>

                        <td>
                            <a href="#" onclick="confirmDelete({{ $media->id }})">Delete</a>

                            <form style="display:none" method="POST" id="delete-form-{{ $media->id }}"
                                  action="{{ route('admin.products.removeMedia', $product->id) }}">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="media_id" value="{{ $media->id }}">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
