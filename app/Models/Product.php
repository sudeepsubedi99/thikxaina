<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function medias()
    {
        return $this->belongsToMany(Media::class);
    }

    /**
     * Show Product Cover Image
     *
     * @return void
     */
    public function getImageAttribute()
    {
        $image = $this->medias->first();
        if ($image)
            return '/storage/' . $image->path;
        else
            return '/images/not-found.png';
    }

    /**
     * Show All Images for that Media
     *
     * @return void
     */
    public function getImagesAttribute()
    {
        $images = $this->medias;
        $all_images = $images->map(function ($image) {
            return $image->path;
        });

        return $all_images;
    }


}
