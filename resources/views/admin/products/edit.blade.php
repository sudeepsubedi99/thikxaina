@extends('adminlte::page')

@section('title', 'Update Product')

@section('content')

<x-alert />

<div class="card">
      <div class="card-header ">
          <h3 class="card-title text-bold">Update Product</h3>
          <div class="card-tools">
            <a href="{{ route('admin.products.index') }}" class="btn btn-primary btn-sm">
            Go Back
        </a>
          </div>
      </div>
      <div class="card-body" >
           <form action="{{ route('admin.products.update', $product->id) }}" method="Post">
             @csrf
             @method('PUT')

             <div class="form-group">
                <label for="name">Name</label>
                <input
                    type="text"
                    name="name" id="name"
                    value="{{ old('name') ?? $product->name }}"
                    class="form-control @error('name') is-invalid @enderror"
                >
                @error('name')
                <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="price">Price</label>
                <input
                    type="number"
                    name="price" id="price"
                    value="{{ old('price') ?? $product->price }}"
                    class="form-control @error('price') is-invalid @enderror"
                >
                @error('price')
                <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea
                    name="description" id="description"
                    value="{{ old('description') ?? '' }}"
                    class="form-control @error('description') is-invalid @enderror"
                >{{ $product->description }}</textarea>
                @error('description')
                <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>


                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-fw fa-save mr-1"></i>
                    Save
                </button>

            </form>

        </div>
  </div>

  @stop
