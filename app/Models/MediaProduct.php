<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediaProduct extends Model
{

    use HasFactory;

    protected $table = 'media_product';

    protected $guarded = [];

    public function media()
    {
        return $this->belongsTo(Media::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
