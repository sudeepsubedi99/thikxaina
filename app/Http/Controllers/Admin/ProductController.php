<?php

namespace App\Http\Controllers\Admin;

use App\Models\Media;
use App\Models\Product;
use App\Models\MediaProduct;
use Illuminate\Http\Request;
use App\Services\MediaService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = Product::Paginate(30);

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required'],
            'price' => ['required', 'numeric', 'gt:0'],
            'description' => ['nullable'],
        ]);

        $product = Product::create($request->only('name', 'price', 'description'));

        return redirect()->route('admin.products.index')
            ->with('success', 'Product created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('admin.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $data = $request->validate([
            'name' => ['required'],
            'price' => ['required', 'numeric', 'gt:0'],
            'description' => ['nullable'],
        ]);

        $product->update($data);

        return redirect()->route('admin.products.index')
            ->with('success', 'Product updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('admin.products.index')
        ->with('success', 'Product deleted successfully!');
    }


    public function media(Product $product)
    {
        return view('admin.products.media', compact('product'));
    }

    public function mediaUpload(Request $request, Product $product)
    {
        $request->validate([
            'image' => 'required|image|mimes:png,gif,jpeg',
        ]);

        $media_id = MediaService::upload($request->file('image'), "products");

        $product->medias()->attach(Media::find($media_id));

        return redirect()
            ->route('admin.products.media', $product->id)
            ->with('success', 'File uploaded successfully!');
    }

    public function removeMedia(Request $request, Product $product)
    {
        $request->validate([
            'media_id' => 'required|exists:media,id',
        ]);

        $media = Media::find($request->media_id);

        $mp = MediaProduct::where('media_id', $media->id)->where('product_id', $product->id)->first();

        if ($mp) {
            Storage::delete("public/" . $media->path);
            $mp->delete();
            $media->delete();
        }

        return redirect()
            ->route('admin.products.media', $product->id)
            ->with('success', 'File removed successfully!');
    }

}
